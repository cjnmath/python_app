#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python


import sys
import requests
import os


def DownloadFile(url, headers={}, path=None, filename=None, proxies=None):
    """ Break a file in to chunks (evrery chunk at most 1024M)
        Download every chunk to
        and then write them to
        filename (str) : 	absolute file name
                            default to the fi
    """
    filename = url.split('/')[-1] if not filename else filename
    path_name = os.path.join(path, filename) if path else os.path.join('./', filename)
    temp_file = requests.get(url, headers=headers, stream=True, proxies=proxies)
    with open(path_name, 'wb') as f:
        for chunk in temp_file.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return temp_file.status_code == requests.codes.ok


def main():
    proxies = {
        'http': 'socks5://127.0.0.1:1080',
        'https': 'socks5://127.0.0.1:1080'
    }

    url = sys.argv[1]
    DownloadFile(url, proxies=proxies)
