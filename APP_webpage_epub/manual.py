#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python

import requests
import urllib
import re


proxies = {
    'http': 'socks5://127.0.0.1:1080',
    'https': 'socks5://127.0.0.1:1080'
}

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0"
}


def clean_alt_bar(line):
    line = re.sub('\|', '--', line)
    line = re.sub(r'/', '<forwardslash>', line)
    line = re.sub(r'\\', '<backwardslash>', line)
    return line


def get_targets():
    temp_lines = open('urls.txt').readlines()
    lines = [line.strip() for line in temp_lines if line.strip()]
    target_names = []
    target_urls = []
    for line_num in range(len(lines)):
        next_num = line_num + 1
        if next_num < len(lines) and lines[next_num].startswith('http'):
            target_names.append(clean_alt_bar(lines[line_num]))
            target_urls.append(lines[next_num])
    return dict(zip(target_names, target_urls))


def DownloadFile(url, headers={}, filename=None, proxies=None):
    """ Break a file in to chunks (evrery chunk at most 1024M)
        Download every chunk to
        and then write them to
        filename (str) : 	absolute file name
                            default to the fi
    """
    local_filename = url.split('/')[-1] if not filename else filename
    temp_file = requests.get(url, headers=headers, stream=True, proxies=proxies)
    with open(local_filename, 'wb') as f:
        for chunk in temp_file.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return temp_file.status_code == requests.codes.ok


def main():
    base_url = 'https://pushtokindle.fivefilters.org/send.php?context=download&format={format}&url={url}'
    targtes = get_targets()
    for name, url in targtes.items():
        data = {
            'format': 'epub',
            # 'url': urllib.parse.quote(url)
            'url': urllib.parse.quote_plus(url)
        }
        download_url = base_url.format(**data)
        count = 0
        ok = False
        while count < 5 and not ok:
            try:
                DownloadFile(download_url, headers=headers, filename=name+'.epub', proxies=proxies)
                ok = True
                print('Finish downloading: ', name+'.epub')
            except:
                ok = False
                count += 1
        if count >= 5:
            print('Somthing wrong during downloading: ', name+'.epub')

if __name__ == '__main__':
    main()
