import os


def check_make_folder(path='download'):
    folder_names = path.split('/')
    main_path = ''
    while folder_names:
        main_path = main_path + '/' + folder_names.pop(0) if main_path else folder_names.pop(0)
        if not os.path.exists(main_path):
            os.mkdir(main_path)
    return main_path
