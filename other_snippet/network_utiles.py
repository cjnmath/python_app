import requests
from lxml.html import fromstring


HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36"
}

MOBILE_HEADERS = {
    "User-Agnet": "Mozilla/5.0 (Linux; Android 7.0; PLUS Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36"
}


def get_response(url, params=None, headers=HEADERS, proxies=None):
    response = requests.get(url, params=params, headers=headers, proxies=proxies)
    return response


def get_root(url, params=None, headers=HEADERS, encoding='utf-8', proxies=None):
    response = get_response(url, params=params, headers=headers, proxies=proxies)
    response.encoding = encoding
    root = fromstring(response.text)
    return root
