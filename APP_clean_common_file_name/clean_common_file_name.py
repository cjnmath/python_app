#! /usr/bin/python3

import os
import glob

files = glob.glob(os.path.join("1", '*'))
common_name = os.path.commonprefix(files)
new_folder_name = os.path.basename(common_name)
if new_folder_name:
    print(new_folder_name)
    for f in files:
        new_name = f.replace(new_folder_name, '')
        os.rename(f, new_name)
        print(new_name)
    os.rename("1", new_folder_name)
    os.mkdir("1")
else:
    print("There is not file in foleder 1.")
